all: Server Client

Server:
	gcc -g -pthread ./server/server.c -o server.out

Client:
	gcc -g -pthread ./client/client.c -o client.out

clean:
	-rm -rf *.out
