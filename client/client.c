/* Gavriil Chaviaras
 * client.c
 */

#include "client_lib.h"

int main(int argc, char **argv){
	struct transfer_prop *prop;
	struct sockaddr_in servAddr;
	char cmd_buf[CMDSIZE];
	char file_name[NAME_MAX];
	int clntSock, port, files, i, id = 0;
	pthread_t client_thr;

	if (argc != 2){
		fprintf(stderr,"Wrong arguments entered.\n");
		return 1;
	}

	// Construct a TCP socket
	if ((clntSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0 ){
		perror("socket");
		exit(EXIT_FAILURE);
	}

	// Construct the server address structure
	memset(&servAddr, 0, sizeof(servAddr));			// Zero out structure
	servAddr.sin_family = AF_INET;					// Internet address family
	servAddr.sin_addr.s_addr= inet_addr(argv[1]);	// Server IP address
	servAddr.sin_port = htons(SERVPORT);			// Server port


	// Establish the connection to the server
	if(connect(clntSock, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0) {
		perror("connect");
		exit(EXIT_FAILURE);
	}

	printf("Connected to %s\n",argv[1]);

	while(1){
		printf("Send:> ");
		scanf(" %[^]\n]s",cmd_buf);	// ignores the white spaces

		to_server(clntSock,cmd_buf,CMDSIZE);

		// extracts the port number (if any) from the cmd
		if (sscanf(cmd_buf,"%*s %*[^]\n]s %d", &port) == EOF)
					port = 65536 - SERVPORT + id;

		// receives the responce from server
		from_server(clntSock,cmd_buf,CMDSIZE);

		if(!strcmp(cmd_buf,"BYE")){			// bye command
			printf("Exiting\n");
			break;
		}
		if(!strcmp(cmd_buf,"TRANSFER")){	// get command

			// initialize properties struct
			prop = (struct transfer_prop*)malloc(sizeof(struct transfer_prop));
			prop->port = port;
			prop->servIP = argv[1];
			prop->clntSock = clntSock;

			// start the thread for the transfer
			if(pthread_create(&client_thr, NULL,
					thread_receive, (void*)prop)){

				printf("Error creating thread\n");
				exit(EXIT_FAILURE);
			}
			id++;
			continue;
		}
		if(!strcmp(cmd_buf,"LIST")){		// ls command
			from_server(clntSock, &files, sizeof(int));
			for(i=0; i<files; i++){
				from_server(clntSock, file_name, NAME_MAX);
				printf("%s\n",file_name);
			}
			continue;
		}

		printf("%s\n",cmd_buf);

	}
	close(clntSock);

	return 0;
}

