/* Gavriil Chaviaras
 * client_lib.h
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>		// for close
#include <arpa/inet.h>	// inet
#include <sys/types.h>
#include <sys/socket.h> // socket
#include <netdb.h>		// IPPROTO_TCP
#include <limits.h>		// NAME_MAX
#include <ctype.h>		// sscanf
#include <pthread.h>	// posix threads

#define SERVPORT 2888
#define BUFSIZE 1024
#define CMDSIZE 128

/* Holds information for file transfer through threads
 */
struct transfer_prop{
	char *servIP;
	int port;
	int clntSock;
};

/* Sends size bytes of data from stream to server
 */
void to_server(int sockfd ,void *stream, int size){

	if (send(sockfd, stream, size, 0) < 0){
		perror("send");
		exit(EXIT_FAILURE);
	}

}

/* Receives size bytes of data in stream from server
 * Returns the size of the received bytes.
 */
int from_server(int sockfd ,void *stream, int size){
	int packetSize;

	packetSize = recv(sockfd, stream, size, 0);
	if( packetSize < 0){
		perror("recv");
		exit(EXIT_FAILURE);
	}

	return packetSize;
}

/* Opens a new file using filename sent from server and starts to
 * write BUFSIZE bytes to the file until server sends 0 bytes.
 */
void receive_file(int sockfd){
	int byteRec = 0;
	char buffer[BUFSIZE];
	char file_name[NAME_MAX];
	FILE *fp;

	from_server(sockfd, file_name, CMDSIZE);

	fp = fopen(file_name,"w");
	if (fp == NULL){
		fprintf(stderr,"Problem opening file\n");
		exit(EXIT_FAILURE);
	}

	printf("\rReceiving %s file.\n",file_name);
	printf("Send:> ");
	fflush(stdout);

	while ( (byteRec = from_server(sockfd, buffer, BUFSIZE)) > 0)
		fwrite(buffer, sizeof(char), byteRec, fp);

	printf("\r%s Received.\n",file_name);
	printf("Send:> ");
	fflush(stdout);

	fclose(fp);
}

/* Establishes a new connection using the properties of the arguments
 * and starts receiving the file using the receive_file function
 */
void *thread_receive(void *arg){
	struct transfer_prop * prop = (struct transfer_prop *)arg;
	struct sockaddr_in servAddr;
	int clntSock;

	// Construct a TCP socket
	if ((clntSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0 ){
		perror("socket");
		exit(EXIT_FAILURE);
	}

	// Construct the server address structure
	memset(&servAddr, 0, sizeof(servAddr));			// Zero out structure
	servAddr.sin_family = AF_INET;					// Internet address family
	servAddr.sin_addr.s_addr= inet_addr(prop->servIP);	// Server IP address
	servAddr.sin_port = htons(prop->port);			// Server port */


	// Establish the connection to the server
	if(connect(clntSock, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0) {
		perror("connect");
		exit(EXIT_FAILURE);
	}


	receive_file(clntSock);	// start receiving file
	close(clntSock);		// close the established connetion
	free(prop);				// free up some space
	pthread_exit(NULL);
}
