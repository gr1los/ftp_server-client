/* Gavriil Chaviaras
 * server.c
 */

#include "server_lib.h"

int main(int argc, char **argv){
	struct sockaddr_in servAddr, clntAddr;
	struct transfer_prop *prop;
	char cmd_buf[CMDSIZE];
	char clnt_ip[INET_ADDRSTRLEN];
	char file_name[NAME_MAX], *file_path, *files;
	int servSock, clntSock;
	int port, cmd_code, len;
	int yes = 1, id = 0;
	pthread_t server_thr;

	if (argc != 2){
		fprintf(stderr,"Wrong arguments entered.\n");
		return 1;
	}

	// Construct a TCP socket
	if ((servSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0){
		perror("socket");
		exit(EXIT_FAILURE);
	}

	#ifdef PORT_REUSE
	if (setsockopt(servSock, SOL_SOCKET, SO_REUSEADDR, &yes,sizeof(int)) == -1) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}
	#endif

	memset(&servAddr, 0, sizeof(servAddr));		 	// Zero out structure
	servAddr.sin_family = AF_INET; 					// Internet address family IPv4
	servAddr.sin_addr.s_addr = htonl(INADDR_ANY);	// Any incoming interface
	servAddr.sin_port = htons(SERVPORT); 			// Local Port defined

	// Bind to the local address
	if (bind(servSock, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0) {
		perror("bind");
		exit(EXIT_FAILURE);
	}

	// Mark the socket so it will listen for incoming connections
	if (listen(servSock, MAXPENDING) < 0) {
		perror("listen");
		exit(EXIT_FAILURE);
	}

	while(1){

		printf("Waiting for a connection.\n");
		len = sizeof(clntAddr);
		// Accept the incoming connection
		if ((clntSock = accept(servSock, (struct sockaddr*) &clntAddr, &len)) < 0 ){
			perror("accept");
			exit(EXIT_FAILURE);
		}

		// Print the client's IP
		inet_ntop(AF_INET, &(clntAddr.sin_addr), clnt_ip, INET_ADDRSTRLEN);
		printf("Hello %s !\n",clnt_ip);

		while(1){

			from_client( clntSock, cmd_buf, CMDSIZE);
			printf("Received:> %s\n", cmd_buf);

			// Find the cmd_code of the received message
			cmd_code = cmd_parse(cmd_buf);

			if( cmd_code == 0){

				fprintf(stderr,
						"Wrong command received.Error code sent.\n");

				to_client(clntSock,"Wrong command try again.", CMDSIZE);
			}else if (cmd_code == 1){		// ls command

				to_client(clntSock,"LIST", CMDSIZE);
				list_dir(clntSock,argv[1]);
			}else if (cmd_code == 2){		// get command
				// initialize properties struct
				prop = (struct transfer_prop*)malloc(sizeof(struct transfer_prop));

				// extracts the file name from the cmd
				sscanf(cmd_buf,"%*s %[^]\n]s %*d",file_name);

				// extracts the port number (if any) from the cmd
				if (sscanf(cmd_buf,"%*s %*[^]\n]s %d",&prop->port) == EOF)
					prop->port = 65536 - SERVPORT + id;

				strcpy(prop->file_path, argv[1]);
				strcat(prop->file_path, file_name);

				// send to client that transfer is starting
				to_client(clntSock,"TRANSFER", CMDSIZE);

				// start the thread for the transfer
				if(pthread_create(&server_thr, NULL,
						thread_sent, (void*)prop)){

					printf("Error creating thread\n");
					exit(EXIT_FAILURE);
				}
				id++;

			}else {						// bye command
				to_client(clntSock,"BYE", CMDSIZE);
				close(clntSock);
				id = 0;
				fprintf(stderr,"Peer %s disconnected.\n",clnt_ip);
				break;
			}
		}
	}

	close(servSock);

	return 0;
}

