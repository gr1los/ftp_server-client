/* Gavriil Chaviaras
 * server_lib.h
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>		// for close
#include <arpa/inet.h>	// inet
#include <sys/types.h>
#include <sys/socket.h> // socket
#include <netdb.h>		// IPPROTO_TCP
#include <dirent.h> 	// readdir
#include <limits.h>		// NAME_MAX , PATH_MAX
#include <ctype.h>		// sscanf
#include <pthread.h> 	// posix threads

#define MAXPENDING 10 // max pending connections on queue
#define SERVPORT 2888
#define BUFSIZE 1024
#define CMDSIZE 128
#define PORT_REUSE	// comment it for non port reuse from sockets

/* Holds information for file transfer through threads
 */
struct transfer_prop{
	char file_path[NAME_MAX + PATH_MAX];
	int port;
	int servSock;
};

/* Parses the message received from client and returns the command code.
 * Else returns zero (0)
 */
int cmd_parse(const char* str){

	char tmp_str[4];
	strncpy(tmp_str,str,3);
	tmp_str[3]='\0';

	if( !strcmp(str,"ls") )
		return 1;

	if( !strcmp(tmp_str,"get") )
		return 2;

	if( !strcmp(str,"bye") )
		return 3;

	return 0;
}

/* Sends size bytes of data from stream to client
 */
void to_client(int sockfd ,void *stream, int size){

	if (send(sockfd, stream, size, 0) < 0){
		perror("send");
		exit(EXIT_FAILURE);
	}

}

/* Receives size bytes of data in stream from client
 * Returns the size of the received bytes.
 */
int from_client(int sockfd ,void *stream, int size){
	int packetSize;

	packetSize = recv(sockfd, stream, size, 0);
	if( packetSize < 0){
		perror("recv");
		exit(EXIT_FAILURE);
	}

	return packetSize;
}

/* Sends to client the files included in the dir
 */
void list_dir(int sockfd, const char* dir){
	char file_name[NAME_MAX];
	struct dirent **filelist;
	int files, i, sym;

	// list the files from the dir using alpha sort
	files = scandir(dir, &filelist, NULL, alphasort);

	if( files < 0){
		perror("scandir");
		exit(EXIT_FAILURE);
	}

	sym = files;
	// find and remove the symbolic links etc
	for (i=0 ;i<files; i++){
		if ( filelist[i]->d_type != DT_REG )
			sym--;
	}

	// send file count
	to_client(sockfd, &sym, sizeof(int));

	// send every file name to client
	for (i=0; i<files; i++){
		if ( filelist[i]->d_type == DT_REG )
			to_client(sockfd, filelist[i]->d_name, NAME_MAX);

		free(filelist[i]);
	}
	free(filelist);

}

/* Opens the file and starts reading it per BUFSIZE bytes and sends them
 * to the client using the port number specificated
 */
void send_file(int sockfd, char *file_path, int port){
	int byteSent = 0;
	char buffer[BUFSIZE];
	char *file_name;
	FILE *fp;

	// file name is the next char from / char of the filepath
	file_name = strrchr(file_path,47) + 1;

	fp = fopen(file_path, "r");
	if (fp == NULL){
		fprintf(stderr,"Problem opening file %s\n",file_path);
		exit(EXIT_FAILURE);
	}

	// send file's name
	to_client(sockfd, file_name, CMDSIZE);

	printf("Sending %s file\n",file_name);

	while ( (byteSent = fread(buffer,sizeof(char), BUFSIZE, fp)) > 0)
		to_client(sockfd, buffer, byteSent);

	printf("%s Sent.\n",file_name);
	fclose(fp);
}


/* Establishes a new connection using the properties of the arguments
 * and starts sending the file using the send_file function
 */
void *thread_sent(void* arg){
	struct transfer_prop * prop = (struct transfer_prop *)arg;
	struct sockaddr_in servAddr, clntAddr;
	int clntSock, servSock, len;
	int yes = 1;

	// Construct a TCP socket
	if ((servSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0){
		perror("socket");
		exit(EXIT_FAILURE);
	}

	#ifdef PORT_REUSE
	if (setsockopt(servSock, SOL_SOCKET, SO_REUSEADDR, &yes,
			sizeof(int)) == -1) {

		perror("setsockopt");
		exit(EXIT_FAILURE);
	}
	#endif

	// Construct the server address structure
	memset(&servAddr, 0, sizeof(servAddr));		 	// Zero out structure
	servAddr.sin_family = AF_INET; 					// Internet address family IPv4
	servAddr.sin_addr.s_addr = htonl(INADDR_ANY);	// Any incoming interface
	servAddr.sin_port = htons(prop->port); 			// Local Port defined

	// Bind to the local address
	if (bind(servSock, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0) {
		perror("bind");
		exit(EXIT_FAILURE);
	}

	// Mark the socket so it will listen for incoming connections
	if (listen(servSock, MAXPENDING) < 0) {
		perror("listen");
		exit(EXIT_FAILURE);
	}

	// Wait to accept the incoming connection
	len = sizeof(clntAddr);
	if ((clntSock = accept(servSock, (struct sockaddr*)&clntAddr, &len)) < 0 ){
		perror("accept");
		exit(EXIT_FAILURE);
	}

	// start sending the file
	send_file(clntSock,prop->file_path,prop->port);

	close(clntSock);		// close the connections
	close(servSock);
	free(prop);				// free up some space


	pthread_exit(NULL);
}
